# vitam-api-proxy

Api REST pour interroger les services ingest de VITAM

## Installation
La documentation suivante est prévu pour une installation sur Linux et testé avec Linux Centos 7.9

### Mise en place du certificat pour accéder à VITAM en HTTPS
Il est recommandé d'ajouter le certificat de l'instance de VITAM au truststore de l'application cliente.
Elle est nécessaire au fonctionnement d'une communication chiffrée.

#### Récupération du certificat du serveur ingest-external de VITAM
```shell
echo | openssl s_client -connect vitam.cines.fr:8443  2>/dev/null | openssl x509 > vitam-ingest-external.cert
```

#### Ajout du certificat au trustStore
On peut soit utiliser le trustStore de la JVM soit en créer un nouveau.
Dans ce dernier cas, il faudra préciser le chemin de ce trustStore dans les options
de la ligne de commande ou via le fichier de configuration

```shell
keytool -import -alias vitam-external -file vitam-external.cert -storetype JKS -keystore server.truststore
```
Si server.truststore n'existe pas, le fichier sera créé. Un mot de passe sera demandé.

La paramètre -alias est libre.

#### Modifier le fichier de configuration application.properties

proxy.api.trustStore-path=/path/to/server.truststore
proxy.api.trustStore-password=password # mot de passe saisie lors de l'import avec keytool

#### Ajout de la clé utilisateur au keystore
Cette clé permet de s'authentifier sur VITAM. Elle est générée par chaque service versant.

##### Création d'une paire de clés (publique et privée)
```shell
keytool -genkey -alias service_versant_alias -keyalg RSA -validity 365 -keystore server.keystore -storetype JKS
```
La paire de clé se trouve dans le fichier server.keystore. I est possible d'ajouter cette paire de clés dans un keystore existant.

##### Extraire la clé publique
```shell
keytool -export -alias service_versant_alias -keystore server.keystore -file sv_public_key.pem
```
Cette clé est à transmettre au administrateur de VITAM.

### Modifier le fichier application.properties
```
proxy.api.keyStore-path=/path/to/server.keystore
proxy.api.keyStore-password=password
```
#### Paramétrer les accès à VITAM
Dans le fichier application.properties
3 points d'accès

```
proxy.api.url-admin.external=https://vitam.cines.fr:8444/admin-external/v1/
proxy.api.url-access.external=https://vitam.cines.fr:8444/access-external/v1/
proxy.api.url-ingest=https://vitam.cines.fr:8443/ingest-external/v1/
```
