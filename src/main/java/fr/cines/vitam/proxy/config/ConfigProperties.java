/**
 Copyright CINES, 2021
 Ce logiciel est régi par la licence CeCILL-C soumise au
 droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C
 telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info". En
 contrepartie de l'accessibilité au code source et des droits de copie, de modification et de
 redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie
 limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du
 programme, le titulaire des droits patrimoniaux et les concédants successifs. A cet égard
 l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation,
 à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant
 donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve
 donc à des développeurs et des professionnels avertis possédant des connaissances informatiques
 approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à
 leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de
 leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de
 sécurité. Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris
 connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
 */

package fr.cines.vitam.proxy.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

public class ConfigProperties {
    private static ConfigProperties INSTANCE;
    private Properties properties;
    
    public static ConfigProperties getInstance(){
        if (INSTANCE==null){
            INSTANCE=new ConfigProperties();
        }
        return INSTANCE;
    }

    private ConfigProperties(){

        String configPath = System.getProperty("fr.cines.vitam.connector.configPath");
        if (configPath == null){
            String cwd = Path.of("").toAbsolutePath().toString();
            configPath = Path.of(cwd,"application.properties").toString();
        }

        File configFile = new File(configPath);

        try {
            FileReader reader = new FileReader(configFile);
            this.properties = new Properties();
            this.properties.load(reader);
            reader.close();
        } catch (FileNotFoundException ex) {
            // TODO Logging
        } catch (IOException ex) {
            // TODO Logging
        }
    }

    public String getUrlAdminExternal(){
        return properties.getProperty("proxy.api.url-admin.external");
    }

    public String getUrlAccessExternal(){
        return properties.getProperty("proxy.api.url-access.external");
    }

    public String getUrlIngest(){
        return properties.getProperty("proxy.api.url-ingest");
    }

    public String getKeyStorePassword(){
        return properties.getProperty("proxy.api.keyStore-password");
    }

    public String getKeyStorePath(){
        return properties.getProperty("proxy.api.keyStore-path");
    }
    
    public String getTrustStorePassword(){
        return properties.getProperty("proxy.api.trustStore-password");
    }
    
    public String getTrustStorePath(){
        return properties.getProperty("proxy.api.trustStore-path");
    }
    
    public String getContratAcces(){
        return properties.getProperty("proxy.api.contrat-acces");
    }

    public String getTenantId(){
        return properties.getProperty("proxy.api.tenant-id");
    }

    public String getDisableHostnameVerification(){
        return properties.getProperty("proxy.api.disable-hostname-verification");
    }
}
