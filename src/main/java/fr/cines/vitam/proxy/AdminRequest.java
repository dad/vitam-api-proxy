/**
 Copyright CINES, 2021
 Ce logiciel est régi par la licence CeCILL-C soumise au
 droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C
 telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info". En
 contrepartie de l'accessibilité au code source et des droits de copie, de modification et de
 redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie
 limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du
 programme, le titulaire des droits patrimoniaux et les concédants successifs. A cet égard
 l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation,
 à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant
 donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve
 donc à des développeurs et des professionnels avertis possédant des connaissances informatiques
 approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à
 leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de
 leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de
 sécurité. Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris
 connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
 */

package fr.cines.vitam.proxy;

import fr.cines.vitam.proxy.request.DSLQuery;
import fr.cines.vitam.proxy.request.RequesterInfo;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class AdminRequest extends AbstractVitamApiProxyRequest {
    public AdminRequest(){
        super();
        this.api_url = configProperties.getUrlAdminExternal();
    }

    /**
     * Request to execute DSL queries against the VitamApi
     * @see <a href="https://www.programmevitam.fr/ressources/DocCourante/raml/externe/dsl.html">DSL API documentation</a>
     * @param requesterInfo
     * @param referential
     * @param dslQuery
     * @return
     * @throws IOException
     * @throws URISyntaxException
     * @throws InterruptedException
     */
    public HttpResponse<String> executeDSL(RequesterInfo requesterInfo, String referential, DSLQuery dslQuery)
        throws IOException, URISyntaxException, InterruptedException {
        return post(requesterInfo, HttpRequest.BodyPublishers.ofString(dslQuery.getDsl_request().toString()), referential, null, "Content-Type", "application/json", "Accept", "application/json");
    }

    /**
     * Request to retrieve the status of a specified archive
     * @param requesterInfo
     * @param requestId The requestId of the archive
     * @return
     * @throws URISyntaxException
     * @throws IOException
     * @throws InterruptedException
     */
    public HttpResponse<String> getArchiveStatus(RequesterInfo requesterInfo, String requestId)
        throws URISyntaxException, IOException, InterruptedException {
        return get(requesterInfo, "operations/" + requestId,null, "Content-Type" + "application/octet-stream");
    }
}
