/**
 Copyright CINES, 2021
 Ce logiciel est régi par la licence CeCILL-C soumise au
 droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C
 telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info". En
 contrepartie de l'accessibilité au code source et des droits de copie, de modification et de
 redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie
 limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du
 programme, le titulaire des droits patrimoniaux et les concédants successifs. A cet égard
 l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation,
 à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant
 donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve
 donc à des développeurs et des professionnels avertis possédant des connaissances informatiques
 approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à
 leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de
 leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de
 sécurité. Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris
 connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
 */

package fr.cines.vitam.proxy;

import fr.cines.vitam.proxy.config.ConfigProperties;
import fr.cines.vitam.proxy.request.RequesterInfo;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public abstract class AbstractVitamApiProxyRequest {

    ConfigProperties configProperties = ConfigProperties.getInstance();
    String api_url;
    static boolean ssl_initialized = false;

    private void initializeSSL(){
        if (!ssl_initialized) {
            System.setProperty("javax.net.ssl.keyStore", configProperties.getKeyStorePath());
            System.setProperty("javax.net.ssl.keyStorePassword", configProperties.getKeyStorePassword());
            System.setProperty("javax.net.ssl.trustStore", configProperties.getTrustStorePath());
            System.setProperty("javax.net.ssl.trustStorePassword", configProperties.getTrustStorePassword());
            System.setProperty("jdk.internal.httpclient.disableHostnameVerification", configProperties.getDisableHostnameVerification());
            ssl_initialized = true;
        }
    }

    public AbstractVitamApiProxyRequest() {
        initializeSSL();
    }

    protected HttpResponse<String> post(RequesterInfo requesterInfo, HttpRequest.BodyPublisher bodyPublisher, String path, String[] parameters, String... headers) throws IOException, InterruptedException, URISyntaxException {
        HttpRequest.Builder httpRequestBuilder = getBuilder(requesterInfo, path, parameters, headers);

        httpRequestBuilder.POST(bodyPublisher);

        return buildAndSendHttpRequest(httpRequestBuilder);
    }

    protected HttpResponse<String> get(RequesterInfo requesterInfo, String path, String[] parameters, String... headers) throws URISyntaxException, IOException, InterruptedException {
        HttpRequest.Builder httpRequestBuilder = getBuilder(requesterInfo, path, parameters, headers);

        httpRequestBuilder.GET();

        return buildAndSendHttpRequest(httpRequestBuilder);
    }

    private HttpResponse<String> buildAndSendHttpRequest(HttpRequest.Builder httpRequestBuilder)
        throws IOException, InterruptedException {
        return HttpClient.newBuilder()
            .build()
            .send(httpRequestBuilder.build(), HttpResponse.BodyHandlers.ofString());
    }

    private HttpRequest.Builder getBuilder(RequesterInfo requesterInfo, String path, String[] parameters, String[] headers) throws URISyntaxException {
        path = path ==null?"": path;

        String contrat_acces;
        String tenant_id;

        if(requesterInfo ==null){
            contrat_acces = configProperties.getContratAcces();
            tenant_id = configProperties.getTenantId();
        } else {
            contrat_acces = (requesterInfo.getContrat_acces() == null) ? configProperties.getContratAcces() : requesterInfo.getContrat_acces();
            tenant_id = (requesterInfo.getTenant_id() == null) ? configProperties.getTenantId() : requesterInfo.getTenant_id();
        }

        HttpRequest.Builder httpRequestBuilder = HttpRequest.newBuilder()
                .uri(new URI(this.api_url + path + paramsToURIParams(parameters)))
                .header("X-Tenant-Id", tenant_id)
                .header("X-Access-Contract", contrat_acces)
                .header("X-Action", "RESUME")
                .header("Accept", "application/json")
                .header("X-Http-Method-Override", "GET");

        if (headers.length != 0 && headers.length%2==0)
            httpRequestBuilder.headers(headers);

        if (requesterInfo != null && requesterInfo.getContextID() != null)
            httpRequestBuilder.header("X-Context-Id", requesterInfo.getContextID());

        return httpRequestBuilder;
    }

    private String paramsToURIParams(String[] params){
        if (params!=null) {
            if (params.length != 0 && params.length % 2 == 0) {
                StringBuilder result = new StringBuilder().append("?");
                for (int i = 0; i < params.length; i += 2) {
                    result.append(params[i]).append("=").append(params[i + 1]).append("&");
                }
                return result.toString();
            }
        }
        return "";
    }

    protected String[] headers(String[] headersA, String[] headersB) {
        if (headersA==null)
            headersA = new String[0];
        if (headersB==null)
            headersB = new String[0];
        int al = headersA.length; int bl = headersB.length;
        String[] result = new String[al + bl];
        System.arraycopy(headersA, 0, result, 0, al);
        System.arraycopy(headersB, 0, result, al, bl);
        return result;
    }

}
